alias l="/bin/ls -AF --group-directories-first --color=auto"
alias l1="/bin/ls -1AF --group-directories-first --color=auto"
alias ll="/bin/ls -Alh --group-directories-first --color=auto"
alias ls="printf \"Use your aliases \\'l\\' or \\'ll\\' you fool!\n\""

alias grep='grep --color=auto --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'
alias fgrep='fgrep --color=auto --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'
alias egrep='egrep --color=auto --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'

alias view="/usr/bin/vim -R"
