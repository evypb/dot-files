GIT_PROMPT="$HOME/.git-prompt.sh"
GIT_COMPLETION="$HOME/.git-completion.bash"
PS1='\[\033]0;${TERM} : $PWD\007\]'             # set window title
PS1="$PS1"'${debian_chroot:+($debian_chroot)}'  # Show chroot (if applicable)
PS1="$PS1"'\[\033[01;32m\]'                     # change to green
PS1="$PS1"'\u@\h '                              # user@host<space>
PS1="$PS1"'\[\033[01;34m\]'                     # change to blue
PS1="$PS1"'\W'                                  # current working directory
if [ -z "$WINELOADERNOEXEC" ]; then
#    test -f "$GIT_PROMPT" || curl -sL https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh > $GIT_PROMPT
#    test -f "$GIT_COMPLETION" || curl -sL https://raw.github.com/git/git/master/contrib/completion/git-completion.bash > $GIT_COMPLETION
    test -f "$GIT_PROMPT" || curl -sL https://github.com/git/git/raw/master/contrib/completion/git-prompt.sh > $GIT_PROMPT
    test -f "$GIT_COMPLETION" || curl -sL https://github.com/git/git/raw/master/contrib/completion/git-completion.bash > $GIT_COMPLETION

    . "$GIT_COMPLETION"
    . "$GIT_PROMPT"
    PS1="$PS1"'\[\033[36m\]'                    # change color to cyan
    PS1="$PS1"'`__git_ps1`'                     # bash function
fi
PS1="$PS1"'\[\033[01;34m\]'                     # change to blue
PS1="$PS1"' \$'                                 # prompt: always $
PS1="$PS1"'\[\033[00m\] '                       # Restore color
