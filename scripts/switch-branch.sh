#!/usr/bin/env bash
set -eux

SCRIPTS_DIR="$(dirname "${BASH_SOURCE[0]}")"
cd "${SCRIPTS_DIR}"
GIT_ROOT="$(git rev-parse --show-toplevel)"
cd "${GIT_ROOT}"

CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
LOCAL_BRANCH=""
if command -v hostname &>/dev/null; then
    LOCAL_BRANCH="$(hostname)"
elif command -v hostnamectl &>/dev/null; then
    LOCAL_BRANCH="$(hostnamectl --static)"
fi

if [[ "${LOCAL_BRANCH:-}" == "" ]]; then
    echo "Failed to determine local branch" >&2
    exit 1
fi

if [[ "${CURRENT_BRANCH}" == "master" ]]; then
    echo "switching to branch '${LOCAL_BRANCH}'"
    git checkout "${LOCAL_BRANCH}"
else
    echo "switching to branch 'master'"
    git checkout master
fi
